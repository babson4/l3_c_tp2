# Server Web HTTP

<img src="https://media.prod.mdn.mozit.cloud/attachments/2015/09/22/11561/526b5bc3d6cc0e62ca5c4d57b47b2982/web-server.svg" width="50%" />

Relation simple Client / Server Web utilisant HTTP réaliser avec le language C standard gnu99

[![version](https://img.shields.io/badge/Version-1.0.0-green.svg)](https://gitlab.com/babson4/l3_c_tp2/-/tags/1.0.0)
[![note](https://badgen.net/badge/icon/%3F%2F20?label=Note)](https://www.upssitech.eu/)
[![Build Status](https://gitlab.com/babson4/l3_c_tp2/badges/master/pipeline.svg)](https://gitlab.com/babson4/l3_c_tp2/-/commits/master)

## Auteurs
👤 **B. Sibout**

- GitLab: [@babson4](https://gitlab.com/babson4)

👤 **E. HOUZANGBE**

## Installation et dépendances

Ce projet utilise:
* [make](https://linux.die.net/man/1/make) (>= 4.3)
* [gcc](https://gcc.gnu.org/) (>= 10.2.0)
* optionnel [oclint](https://docs.oclint.org/en/stable/) (>= 20.11)
* optionnel [valgrind](http://valgrind.org) (>= 3.16.1)

```bash
#Creation du fichier dans le home directory par simplicité, il peut être déplacé
mkdir ~/server_http_c
git clone https://gitlab.com/babson4/l3_c_tp2 ~/server_http_c/

# Installation des dépendances optionnels
# oclint 20.11
wget -q -O /opt/oclint.tar.gz https://github.com/oclint/oclint/releases/download/v20.11/oclint-20.11-llvm-11.0.0-x86_64-linux-ubuntu-20.04.tar.gz
tar xzvf /opt/oclint.tar.gz -C /opt/
export PATH=/opt/oclint-20.11/bin:$PATH

# Valgrind
sudo apt install valgrind
```

## 🚀Utilisation

Mettre tout les fichiers web dans `src/server/file/` (HTML/JS/CSS pris en charge)

```bash
cd ~/server_http_c
make help
make
bin/serverMain
```

Pour généré le rapport Oclint :
`make oclint/(client/server)` le rapport sera dans doc/

## ⭐️FAQ

Lors de l'execution de `make valgrind/server` j'ai un message d'erreur me disant `bash: valgrind : commande introuvable`
>  `apt install valgrind`

## 📝 License

Copyright © 2020 B. Sibout && E. HOUZANGBE

This project is MIT licensed.

<img src="https://stri-online.net/FTLV/pluginfile.php/1/theme_adaptable/adaptablemarketingimages/0/Logos.png" width="50%"/>
