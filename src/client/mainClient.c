#include "client.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  char *message;

  // on se connect au serveur avec le protocole HTTP
  if (InitialisationAvecService("www.wwwdotcom.com", "http") != 1) {
    printf("Erreur d'initialisation\n");
    return 1;
  }

  // on fait une requete HTTP GET pour demander le contenue de la racine en
  // faisant attention au retour chariot
  if (Emission("GET / HTTP/1.1\n") != 1) {
    printf("Erreur d'émission\n");
    return 1;
  }

  // On précise l'host voulu
  if (Emission("Host: www.wwwdotcom.com\n\n") != 1) {
    printf("Erreur d'émission\n");
    return 1;
  }

  // contenue du message
  message = Reception();

  if (message != NULL) {
    // lecture du message
    while (message != NULL) {
      printf("%s\n", message);
      // gestion de mémoire; mis dans le fichier initial donc je le laisse.
      free(message);
      // recuperation de la nouvelle ligne de message
      message = Reception();
    };
  } else {
    printf("Erreur de reception\n");
    return 1;
  }

  // fermeture de la socket (fin de la liaison client / serveur)
  Terminaison();

  return 0;
}
