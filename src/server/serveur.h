#ifndef __SERVEUR_H__
#define __SERVEUR_H__
#include <stddef.h>

/* Initialisation.
 * Creation du serveur.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int Initialisation();

/* Initialisation.
 * Creation du serveur en precisant le service ou numero de port.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int InitialisationAvecService(char *service);

/* Attends qu'un client se connecte.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int AttenteClient();

/* Recoit un message envoye par le client.
 * retourne le message ou NULL en cas d'erreur.
 * Note : il faut liberer la memoire apres traitement.
 */
char *Reception();

/* Envoie un message au client.
 * Attention, le message doit etre termine par \n
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int Emission(char *message);

/* Recoit des donnees envoyees par le client.
 * renvoie le nombre d'octets reeus, 0 si la connexion est fermee,
 * un nombre negatif en cas d'erreur
 */
int ReceptionBinaire(char *donnees, size_t tailleMax);

/* Envoie des donnees au client en precisant leur taille.
 * renvoie le nombre d'octets envoyes, 0 si la connexion est fermee,
 * un nombre negatif en cas d'erreur
 */
int EmissionBinaire(char *donnees, size_t taille);

/* Ferme la connexion avec le client.
 */
void TerminaisonClient();

/* Arrete le serveur.
 */
void Terminaison();

////// fonction ajoutee ////////::

/* Extrait le nom du fichier demandé d'une requete HTTP contenue dans une chaîne
 * de caractères */
int extraitFichier(char *requete, size_t longueurRequete, char *nomFichier,
                   size_t maxNomFichier);

/* Calcule la longueur d'un fichier */
size_t longueur_fichier(char *nomFichier);

char *typeRequete(char *requete);

int envoyerReponseErreur(int numErreur);

int envoyerReponse200(char *nomFichier);
#endif
