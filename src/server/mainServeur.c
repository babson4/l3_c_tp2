#include "serveur.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char nom[255];
    int codeReponse = 0;

    Initialisation();

    while (1) {
        AttenteClient();

        // on attend de recevoir la requete en entier
        char *requete = calloc(5120, sizeof(char));
        int fini = 0;
        do {
            char *morceauCourant = Reception();
            if (morceauCourant == NULL)
                break;
            if (strcmp(morceauCourant, "\r\n") == 0)
                fini = 1;
            else
                requete = strcat(requete, morceauCourant);

            free(morceauCourant);
        } while (!fini);

        // on recupère le type de requete: (POST,GET,PUT,HEAD,DELETE.....)
        char *typeReq = typeRequete(requete);

        // controle du type de retour
        if (strcmp(typeReq, "GET")) {
            // 501: Not Implemented selon les codes de réponse HTTP
            envoyerReponseErreur(501);
            // Affichage de l'erreur
            fprintf(stderr, "Type de requete reçue : %s\n", typeReq);
        } else {
            // On traite la demande GET
            if ((codeReponse =
                     extraitFichier(requete, strlen(requete), nom, 255)) < 0) {
                // on envoie le code de retour selon le resultat de la fonction
                envoyerReponseErreur(abs(codeReponse));
            } else {
                // On affiche le contenue de la page
                int len = longueur_fichier(nom);
                if (len <= -1) {
                    // 404 Not found
                    envoyerReponseErreur(404);
                    fprintf(stderr, "-- Fichier introuvable: %s\n", nom);
                } else {
                    envoyerReponse200(nom);
                }
            }
        }
        free(requete);
        free(typeReq);
        TerminaisonClient();
    }

    return 0;
}
