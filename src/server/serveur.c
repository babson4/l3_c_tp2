#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>
#endif

#include <errno.h>

#include "serveur.h"

#define TRUE 1
#define FALSE 0
#define LONGUEUR_TAMPON 4096

#ifdef WIN32
#define perror(x) printf("%s : code d'erreur : %d\n", (x), WSAGetLastError())
#define close closesocket
#define socklen_t int
#endif

/* Variables cachees */

/* le socket d'ecoute */
int socketEcoute;
/* longueur de l'adresse */
socklen_t longeurAdr;
/* le socket de service */
int socketService;
/* le tampon de reception */
char tamponClient[LONGUEUR_TAMPON];
int debutTampon;
int finTampon;

/* Initialisation.
 * Creation du serveur.
 */
int Initialisation() { return InitialisationAvecService("13214"); }

/* Initialisation.
 * Creation du serveur en precisant le service ou numero de port.
 * renvoie 1 si ea c'est bien passe 0 sinon
 */
int InitialisationAvecService(char *service) {
    int n;
    const int on = 1;
    struct addrinfo hints, *res, *ressave;

#ifdef WIN32
    WSADATA wsaData;
    if (WSAStartup(0x202, &wsaData) == SOCKET_ERROR) {
        printf("WSAStartup() n'a pas fonctionne, erreur : %d\n",
               WSAGetLastError());
        WSACleanup();
        exit(1);
    }
    memset(&hints, 0, sizeof(struct addrinfo));
#else
    bzero(&hints, sizeof(struct addrinfo));
#endif

    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if ((n = getaddrinfo(NULL, service, &hints, &res)) != 0) {
        printf("Initialisation, erreur de getaddrinfo : %s", gai_strerror(n));
        return 0;
    }
    ressave = res;

    do {
        socketEcoute =
            socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (socketEcoute < 0)
            continue; /* error, try next one */

        setsockopt(socketEcoute, SOL_SOCKET, SO_REUSEADDR, (char *)&on,
                   sizeof(on));
#ifdef BSD
        setsockopt(socketEcoute, SOL_SOCKET, SO_REUSEPORT, &on, sizeof(on));
#endif
        if (bind(socketEcoute, res->ai_addr, res->ai_addrlen) == 0)
            break; /* success */

        close(socketEcoute); /* bind error, close and try next one */
    } while ((res = res->ai_next) != NULL);

    if (res == NULL) {
        perror("Initialisation, erreur de bind.");
        return 0;
    }

    /* conserve la longueur de l'addresse */
    longeurAdr = res->ai_addrlen;

    freeaddrinfo(ressave);
    /* attends au max 4 clients */
    listen(socketEcoute, 4);
    printf("Creation du serveur reussie sur %s.\n", service);

    return 1;
}

/* Attends qu'un client se connecte.
 */
int AttenteClient() {
    struct sockaddr *clientAddr;
    char machine[NI_MAXHOST];

    clientAddr = (struct sockaddr *)malloc(longeurAdr);
    socketService = accept(socketEcoute, clientAddr, &longeurAdr);
    if (socketService == -1) {
        perror("AttenteClient, erreur de accept.");
        return 0;
    }
    if (getnameinfo(clientAddr, longeurAdr, machine, NI_MAXHOST, NULL, 0, 0) ==
        0) {
        printf("Client sur la machine d'adresse %s connecte.\n", machine);
    } else {
        printf("Client anonyme connecte.\n");
    }
    free(clientAddr);
    /*
     * Reinit buffer
     */
    debutTampon = 0;
    finTampon = 0;

    return 1;
}

/* Recoit un message envoye par le serveur.
 */
char *Reception() {
    char message[LONGUEUR_TAMPON];
    int index = 0;
    int fini = FALSE;
    int retour = 0;
    while (!fini) {
        /* on cherche dans le tampon courant */
        while ((finTampon > debutTampon) &&
               (tamponClient[debutTampon] != '\n')) {
            message[index++] = tamponClient[debutTampon++];
        }
        /* on a trouve ? */
        if ((index > 0) && (tamponClient[debutTampon] == '\n')) {
            message[index++] = '\n';
            message[index] = '\0';
            debutTampon++;
            fini = TRUE;
#ifdef WIN32
            return _strdup(message);
#else
            return strdup(message);
#endif
        } else {
            /* il faut en lire plus */
            debutTampon = 0;
            retour = recv(socketService, tamponClient, LONGUEUR_TAMPON, 0);
            if (retour < 0) {
                perror("Reception, erreur de recv.");
                return NULL;
            } else if (retour == 0) {
                fprintf(stderr, "Reception, le client a ferme la connexion.\n");
                return NULL;
            } else {
                /*
                 * on a recu "retour" octets
                 */
                finTampon = retour;
            }
        }
    }
    return NULL;
}

/* Envoie un message au client.
 * Attention, le message doit etre termine par \n
 */
int Emission(char *message) {
    int taille;
    if (strstr(message, "\n") == NULL) {
        fprintf(stderr, "Emission, Le message n'est pas termine par \\n.\n");
        return 0;
    }
    taille = strlen(message);
    if (send(socketService, message, taille, 0) == -1) {
        perror("Emission, probleme lors du send.");
        return 0;
    }
    printf("Emission de %d caracteres.\n", taille + 1);
    return 1;
}

/* Recoit des donnees envoyees par le client.
 */
int ReceptionBinaire(char *donnees, size_t tailleMax) {
    size_t dejaRecu = 0;
    int retour = 0;
    /* on commence par recopier tout ce qui reste dans le tampon
     */
    while ((finTampon > debutTampon) && (dejaRecu < tailleMax)) {
        donnees[dejaRecu] = tamponClient[debutTampon];
        dejaRecu++;
        debutTampon++;
    }
    /* si on n'est pas arrive au max
     * on essaie de recevoir plus de donnees
     */
    if (dejaRecu < tailleMax) {
        retour =
            recv(socketService, donnees + dejaRecu, tailleMax - dejaRecu, 0);
        if (retour < 0) {
            perror("ReceptionBinaire, erreur de recv.");
            return -1;
        } else if (retour == 0) {
            fprintf(stderr,
                    "ReceptionBinaire, le client a ferme la connexion.\n");
            return 0;
        } else {
            /*
             * on a recu "retour" octets en plus
             */
            return dejaRecu + retour;
        }
    } else {
        return dejaRecu;
    }
}

/* Envoie des donnees au client en precisant leur taille.
 */
int EmissionBinaire(char *donnees, size_t taille) {
    int retour = 0;
    retour = send(socketService, donnees, taille, 0);
    if (retour == -1) {
        perror("Emission, probleme lors du send.");
        return -1;
    } else {
        return retour;
    }
}

////////////////// FONCTION AJOUTEE ///////////////////

// Merci Fabien pour les fonction utile(decodeURL et ishex), il les a également
// pris de qq'un d'autre :D
int ishex(int x) {
    return (x >= '0' && x <= '9') || (x >= 'a' && x <= 'f') ||
           (x >= 'A' && x <= 'F');
}

int decodeURL(const char *source, char *dest) {
    char *o;
    const char *end = source + strlen(source);
    int c;

    for (o = dest; source <= end; o++) {
        c = *source++;
        if (c == '+')
            c = ' ';
        else if (c == '%' && (!ishex(*source++) || !ishex(*source++) ||
                              !sscanf(source - 2, "%2d", &c)))
            return -1;
        if (dest)
            *o = c;
    }
    return o - dest;
}

char *typeRequete(char *requete) {
    // on recupère le type de requete: (POST,GET,PUT,HEAD,DELETE.....)
    int curseur = (int)strcspn(requete, " ");

    // on stock le resutlat +s1 (\0)
    char *typeReq = calloc(curseur + 1, sizeof(char));
    // recopie la chaine requete dans typeReq avec curseur char max
    strncat(typeReq, requete, curseur);
    return typeReq;
}

int extraitFichier(char *requete, size_t longueurRequete, char *nomFichier,
                   size_t maxNomFichier) {
    // on recupère le type de requete: (POST,GET,PUT,HEAD,DELETE.....)
    char *typeReq = typeRequete(requete);
    int lenTypeReq = strlen(typeReq) + 1;
    char *tempReturn = calloc(255, sizeof(char));

    // On a la position du dernier caractère du fichier
    int positionFinNomFichier = (int)strcspn(requete + lenTypeReq, " ");

    // controle du résulat
    if (positionFinNomFichier >= (int)longueurRequete) {
        fprintf(stderr, "fichier introuvable.\n");
        // toujours liberer la mémoire d'une alloc
        free(typeReq);
        return -400;
    } else if (positionFinNomFichier - 2 > (int)maxNomFichier) {
        //-2 car on a \0 et le /
        fprintf(stderr, "Nom de fichier trop long.\n");
        // toujours liberer la mémoire d'une alloc
        free(typeReq);
        return -414;
    }

    if (positionFinNomFichier == 1) {
        *tempReturn = '\0';
    } else {
        // On exclu le / et on extrait le fichier de la requête
        strncpy(tempReturn, requete + lenTypeReq + 1,
                positionFinNomFichier - 1);
    }

    *(tempReturn + positionFinNomFichier - 1) =
        '\0'; // On ajoute le char de fin car strncpy ne le fait pas.

    decodeURL(tempReturn, nomFichier);
    // toujour liberer la mémoire
    free(typeReq);
    free(tempReturn);
    return 0;
}

size_t longueur_fichier(char *nomFichier) {
    int taille;
    FILE *fichier = NULL;

    fprintf(stderr, "%s\n", nomFichier);

    // controle de l'ouverture du fichier
    fichier = fopen(nomFichier, "r");
    if (fichier == NULL) {
        fprintf(stderr, "Erreur sur l'ouverture du fichier, exit -1\n");
        return (-1);
    }

    // on place le curseur à la fin en verrifiant le bon déroulement
    if ((fseek(fichier, 0, SEEK_END)) != 0) {
        fprintf(stderr, "Erreur sur lecture du fichier, exit -2\n");
        fclose(fichier);
        return (-2);
    }

    // on recupère l'emplacement du curseur en verrifiant le bon déroulement
    if ((taille = ftell(fichier)) == -1) {
        fprintf(stderr, "Erreur sur lecture du fichier, exit -3\n");
        fclose(fichier);
        return (-3);
    }

    // fermeture du fichier
    fclose(fichier);
    return (taille);
}

int envoyerReponseErreur(int numErreur) {

    char enTete[50];
    char message[50];

    switch (numErreur) {
    case 400:
        strcpy(enTete, "400 Bad Request");
        strcpy(message, "ERREUR : Demande mal formée");
        break;

    case 404:
        strcpy(enTete, "404 Not Found");
        strcpy(message, "ERREUR : Le fichier demandé est introuvable");
        break;

    case 414:
        strcpy(enTete, "414 URI Too Long");
        strcpy(message, "Erreur : URI trop long");
        break;

    case 501:
        strcpy(enTete, "501 Not Implemented");
        strcpy(message, "Erreur : Le serveur ne prend en compte que du GET");
        break;

        // non utile dans notre cas, mais l'est en situation réelle
    default:
        strcpy(enTete, "500 Server Error");
        strcpy(message, "ERREUR serveur générique");
        numErreur = 500;
        break;
    }

    char textReturn[100];
    sprintf(textReturn, "Erreur %d\n\n%s", numErreur, message);

    char header[100];
    sprintf(
        header,
        "HTTP/1.1 %s\r\nContent-type: text/html\r\nContent-length: %d\r\n\r\n",
        enTete, (int)strlen(textReturn));

    // controle de l'envoie
    if (!Emission(header) || !Emission(textReturn)) {
        return 0;
    }

    return 1;
}

int envoyerContenuFichier(char *nomF) {
    // ouverture et verrif du fichier
    FILE *file = NULL;
    file = fopen(nomF, "r");
    if (file == NULL)
        return 0;

    int maxLen = longueur_fichier(nomF) + 1;
    char line[maxLen];
    char *pntrLine = NULL;

    while ((pntrLine = fgets(line, maxLen, file)) != NULL) {

        // Si pLine ne termine pas par un '\n'
        if (strstr(pntrLine, "\n") == NULL) {
            strcat(pntrLine, "\n");
        }
        if (!Emission(pntrLine)) {
            free(pntrLine);
            return 0;
        }
    }

    free(pntrLine);
    return 1;
}

int envoyerContenuFichierBinaire(char *nomF) {
    // on ouvre le fichier en question
    FILE *file = NULL;
    file = fopen(nomF, "rb"); // ouverture en mode binaire
    // controle de l'ouverture
    if (file == NULL)
        return 0;

    // alloc de la chaine qui recoit les info
    char *binF = calloc(longueur_fichier(nomF), sizeof(char));

    // ecriture sur la chaine
    fread(binF, longueur_fichier(nomF) * sizeof(char), 1, file);

    // envoie au client
    if (!EmissionBinaire(binF, longueur_fichier(nomF))) {
        fclose(file);
        free(binF);
        return 0;
    }

    fclose(file);
    free(binF);

    return 0;
}

int envoyerReponse200(char *nomF) {

    char mimeType[50];
    // quel type de fichier ?
    // attention avec les verrification de ce type:
    // dans le cas d'un upload sur un serveur, des fichier script.php.png
    // pourrait passer, ce qui donne le libre cours aux attaques
    // malveillantes
    char *fileExtension = strrchr(nomF, '.');
    int binaire = 0; // bool pour envoie binaire

    /////////////////////// MIME image ///////////////////////
    if (strcmp(fileExtension, ".jpg") == 0 ||
        strcmp(fileExtension, ".jpeg") == 0) {
        binaire = 1;
        strcpy(mimeType, "image/jpeg");
    } else if (strcmp(fileExtension, ".png") == 0) {
        binaire = 1;
        strcpy(mimeType, "image/png");
    } else if (strcmp(fileExtension, ".ico") == 0) {
        binaire = 1;
        strcpy(mimeType, "image/vnd.microsoft.icon");
    } else if (strcmp(fileExtension, ".gif") == 0) {
        binaire = 1;
        strcpy(mimeType, "image/gif");
    } else if (strcmp(fileExtension, ".png") == 0) {
        binaire = 1;
        strcpy(mimeType, "image/png");
        /////////////////////// MIME text ///////////////////////
    } else if (strcmp(fileExtension, ".txt") == 0) {
        strcpy(mimeType, "text/plain");
    } else if (strcmp(fileExtension, ".html") == 0) {
        strcpy(mimeType, "text/html");
    } else if (strcmp(fileExtension, ".js") == 0) {
        strcpy(mimeType, "text/javascript");
    } else if (strcmp(fileExtension, ".css") == 0) {
        strcpy(mimeType, "text/css");
        /////////////////////// MIME application ///////////////////////
    } else if (strcmp(fileExtension, ".pdf") == 0) {
        binaire = 1;
        strcpy(mimeType, "application/pdf");
    } // reste audio / video / multipart
    else {
        // on utilise octet application/octet-stream
        // il est décrit comme un recours pour les sous-types et les types
        // non reconnus.
        binaire = 1;
        strcpy(mimeType, "application/octet-stream");
    }

    // meme traitement que envoyerReponseErreur en ajoutant le media
    char header[150];
    sprintf(header,
            "HTTP/1.1 200 OK\r\nContent-type:%s\r\nContent-length: %d\r\n\r\n",
            mimeType, (int)longueur_fichier(nomF));
    if (!Emission(header))
        return 0;

    if (binaire) {
        if (!envoyerContenuFichierBinaire(nomF))
            return 0;
    } else {
        if (!envoyerContenuFichier(nomF))
            return 0;
    }

    return 1;
}

////////////////////////////////////////////////////////

/* Ferme la connexion avec le client. */
void TerminaisonClient() { close(socketService); }

/* Arrete le serveur. */
void Terminaison() { close(socketEcoute); }
