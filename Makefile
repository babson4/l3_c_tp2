# Color prefix for Linux distributions
COLOR_PREFIX := e

ifeq ($(shell uname -s),Darwin)
	COLOR_PREFIX := 033
endif

######################### COLORS #########################
# Color definition for print purpose
BROWN=\$(COLOR_PREFIX)[0;33m
BLUE=\$(COLOR_PREFIX)[1;34m
RED=\$(COLOR_PREFIX)[1;31m
GREEN=\$(COLOR_PREFIX)[1;32m
END_COLOR=\$(COLOR_PREFIX)[0m


######################### DIRECTORIES #########################
# Source code directory structure
BINDIR := bin
LOGDIR := log
LIBDIR := lib
SRCDIR_CLIENT := src/client
SRCDIR_SERVER := src/server

######################### COMPILER #########################
# Defines the C Compiler
CC := gcc

# Defines the language standards for GCC
STD := -std=gnu99

# Protection for stack-smashing attack
STACK := -fstack-protector-all -Wstack-protector

# Specifies to GCC the required warnings
WARNS := -Wall -Wextra -pedantic

# Flags for compiling
CFLAGS := -O3 $(STD) $(STACK) $(WARNS)

# Debug options
DEBUG := -g3 -DDEBUG=1

# Dependency libraries
LIBS := # -lm  -I some/path/to/library

######################### CLIENT #########################
# %.o file names
NAMES_CLIENT :=  $(wildcard $(SRCDIR_CLIENT)/*.c) #get all .c file
OBJECTS_CLIENT := $(NAMES_CLIENT:.c=.o) #replace all .c with .o
EXEC_CLIENT := $(BINDIR)/clientMain
######################### SERVER #########################
# %.o file names server
NAMES_SERVER := $(wildcard $(SRCDIR_SERVER)/*.c)
OBJECTS_SERVER := $(NAMES_SERVER:.c=.o)
EXEC_SERVER := $(BINDIR)/serverMain

###########################################################################
########################### COMPILATION RULES #############################
###########################################################################

default: all

# Help message
help:
	@echo "Aide du make pour le serveur HTTP"
	@echo
	@echo "Regles disponibles:"
	@echo "    all      		- Compile et genere tous les binaires"
	@echo "    $(EXEC_CLIENT)   	- Compile et genere le binaire du client"
	@echo "    $(EXEC_SERVER)   	- Compile et genere le binaire du serveur"
	@echo "    valgrind/client 	- Valgrind sur le binaire client"
	@echo "    valgrind/server 	- Valgrind sur le binaire server"
	@echo "    oclint/client 	- Rapport du linter sur le client"
	@echo "    oclint/server 	- Rapport du linter sur le server"
	@echo "    clean    		- Supprime les fichiers binaires, objets et les logs"
	@echo "    help     		- Affiche cette aide"

#complie all files
all:
	@echo "$(BROWN)Compilation pour : make $(SRCDIR_CLIENT) $(END_COLOR)";
	@$(MAKE) $(EXEC_CLIENT)
	@echo "\n--\nBinaire $(BLUE)client$(END_COLOR) généré dans : $(BROWN)$(EXEC_CLIENT)$(END_COLOR)\n";
	@echo
	@echo "$(BROWN)Compilation pour : make $(SRCDIR_SERVER) $(END_COLOR)";
	@$(MAKE) $(EXEC_SERVER)
	@echo "\n--\nBinaire $(BLUE)server$(END_COLOR) généré dans : $(BROWN)$(EXEC_SERVER)$(END_COLOR)\n";
	@echo
	@echo

#rule for client
$(EXEC_CLIENT): $(OBJECTS_CLIENT)
	@$(CC) -o $@ $^ $(CFLAGS)
	mv $(SRCDIR_CLIENT)/*.o $(LIBDIR)/

#rule for server
$(EXEC_SERVER): $(OBJECTS_SERVER)
	@$(CC) -o $@ $^ $(CFLAGS)
	mv $(SRCDIR_SERVER)/*.o $(LIBDIR)/

# rules for all .o files
%.o: %.c
	@$(CC) -o $@ -c $^ $(CFLAGS)

# valgrind rules
valgrind/client:
	valgrind --track-origins=yes --leak-check=full --leak-resolution=high \
		--log-file=$(LOGDIR)/client.log $(EXEC_CLIENT)
	@echo "\nFichier log client dans: $(LOGDIR)/client.log\n";

valgrind/server:
	valgrind --track-origins=yes --leak-check=full --leak-resolution=high \
		--log-file=$(LOGDIR)/server.log $(EXEC_SERVER)
	@echo "\nFichier log server dans: $(LOGDIR)/server.log\n"

# rule for oclint
oclint/client:
	oclint -max-priority-1=9999 -max-priority-2=9999 -max-priority-3=9999 -report-type html -o doc/oclint_report_client.html $(SRCDIR_CLIENT)/*.c -- -03 -std=gnu99 -fstack-protector-all -Wstack-protector -Wall -Wextra -pedantic

oclint/server:
	oclint -max-priority-1=9999 -max-priority-2=9999 -max-priority-3=9999 -report-type html -o doc/oclint_report_server.html $(SRCDIR_SERVER)/*.c -- -03 -std=gnu99 -fstack-protector-all -Wstack-protector -Wall -Wextra -pedantic

# Rule for cleaning the project
clean:
	@rm -rvf $(BINDIR)/* $(LIBDIR)/* $(LOGDIR)/*;
